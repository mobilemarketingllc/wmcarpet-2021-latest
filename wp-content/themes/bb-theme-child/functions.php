<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');



function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

if(@$_GET['keyword'] != '' && @$_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , $_GET['keyword']);
	    setcookie('brand' , $_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if(@$_GET['brand'] !="" && @$_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , $_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if(@$_GET['brand'] =="" && @$_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , $_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( @$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "")
   {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring *<h1>';
   }
   else
   {
       $keyword = $_COOKIE['keyword'];
	   $brand = $_COOKIE['brand'];
    //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
   }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	  var brand_val ='<?php echo @$_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo @$_COOKIE['keyword'];?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script>
  <?php  
     setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600);
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php 
}


$data ='';

$ddsa = unserialize(stripslashes($data));

wp_clear_scheduled_hook( '404_redirection_log_cronjob' );
 wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('bmg_404_redirection_301_log_cronjob')) {
    
        wp_schedule_event( time() +  17800, 'daily', 'bmg_404_redirection_301_log_cronjob');
}

add_action( 'bmg_404_redirection_301_log_cronjob', 'bmg_custom_404_redirect_hook' ); 

//add_action( '404_redirection_log_cronjob', 'custom_404_redirect_hook' ); 


function bmg_check_404($url) {
   $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function bmg_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/flooring/carpet/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/flooring/hardwood/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/flooring/laminate/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url_lvt = '/flooring/vinyl/products/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url_lvt = '/flooring/vinyl/products/';
            }
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url_tile = '/flooring/tile/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_tile,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }
 function special_coretec_slider($atts) {
	// query is made
    $args = array(
        "post_type" => 'luxury_vinyl_tile',
        "post_status" => "publish",
        'meta_query' => array(
              array(
                  'key' => 'featured',
                  'compare' => '==',
                  'value' => '1'
              ),
             
        ),
        "orderby" => "title",
        "order" => "ASC",
        "posts_per_page" => 10
      );
	$q = new WP_Query($args);
	if($q->have_posts()) {
	ob_start();

?>
	<div class="product-grid swatch specialSlider" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 
           $col_class = 'col-md-3 col-sm-4 col-xs-6';
           $show_financing = get_option('sh_get_finance');

    if(get_option('salesbrand')!=''){
            $slide_brands = rtrim(get_option('salesbrand'), ",");
            $brandonsale = array_filter(explode(",",$slide_brands));
            $brandonsale = array_map('trim', $brandonsale);
        }
    ?>

    
<?php while ( $q->have_posts() ): $q->the_post(); ?>
     <?php if(get_field('swatch_image_link')) {
    
    //collection field
    $collection = get_field('collection', get_the_ID());
    $brand =  get_field('brand',get_the_ID());
    ?>
    <div class="<?php echo $col_class; ?>">
    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
						$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');							
							
					?>
            <img class="search-pro-img" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>      
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php the_field('collection'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>		
			 
			 <?php if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){  ?>
                <a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="fl-button getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>
            <?php if($show_financing == 1){?>
            <a href="<?php echo site_url(); ?>/flooring-financing/" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text">GET FINANCING</span>
            </a><br />
           <?php } ?>
            <a class="link plp-view-product" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
    <?php } ?>
    <?php endwhile; ?>
</div>
</div>
<?php

	$output_string = ob_get_contents();
	ob_end_clean();
	
    wp_reset_query();
    return $output_string;
}
}

add_shortcode('SPECIAL_CORETEC_SLIDER', 'special_coretec_slider');


function coretec_featured_products_slider() {

    // print_r($atts);
 
     $args = array(
         'post_type' => 'luxury_vinyl_tile',        
         'posts_per_page' => 8,
         'orderby' => 'rand',
         'meta_query' => array(
             array(
                 'key' => 'featured', 
                 'value' => "1", 
                 'compare' => '=='
             )
         ) 
     );
     $query = new WP_Query( $args );
     if( $query->have_posts() ){
         $outpout = '<div class="coretec_slider featured-products"><div class="featured-product-list">';
         while ($query->have_posts()) : $query->the_post(); 
             $gallery_images = get_field('gallery_room_images');
             $gallery_img = explode("|",$gallery_images);
            
             // loop through the rows of data

             $room_image =  single_gallery_image_product(get_the_ID(),'600','400');
            
 
             $color_name = get_field('color');
             $brand_name = get_field('brand');
             $collection = get_field('collection');
             $post_type = get_post_type_object(get_post_type( get_the_ID() ));
            
             
             $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="'.$room_image.'" alt="'.get_the_title().'" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="'.get_the_permalink().'" class="button alt viewproduct-btn">View Product</a>';
              
            if( get_option('getcouponbtn') == 1){

                $outpout .= '<a href="/flooring-coupon/" class="button fea-coupon_btn">Get Coupon</a>';
            }
              
              $outpout .= '</div>
                         </div>
                     </div>
                     <div class="product-info">
                         <div class="slider-product-title"><a href="'.get_the_permalink().'">'.$collection.'</a></div>
                         <h3 class="floorte-color">'.$color_name.'</h3>
                     </div>
                 </div>
             </div>';
 
         endwhile;
         $outpout .= '</div></div>';
         wp_reset_query();
     }  
 
 
     return $outpout;
 }
 add_shortcode( 'coretec_featured_products_slider', 'coretec_featured_products_slider' );
<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// // SS - Coupon - $500oFF
add_action( 'gform_after_submission_14', 'post_to_third_party_14', 10, 2 );

function post_to_third_party_14( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = 'dc35b4a4-a892-4a8b-840d-ff2b161bd83b';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '2' ),
        'terms' => rgar( $entry, '7' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//SS-contact us
add_action( 'gform_after_submission_15', 'post_to_third_party_15', 10, 2 );

function post_to_third_party_15( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = '23b20b7d-7053-4fe1-bd29-60aa4c3e0cf7';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'number' => rgar( $entry, '4' ),
        'questions' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//SS - Carpet Cleaning Lead
add_action( 'gform_after_submission_17', 'post_to_third_party_17', 10, 2 );

function post_to_third_party_17( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = 'f03832c9-db0f-48f8-a79a-1d50c5886f32';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'zip' => rgar( $entry, '5' ),
        'rooms' => rgar( $entry, '11' ),
        'steps' => rgar( $entry, '14' ),
        'date' => rgar( $entry, '13' ),
        'time' => rgar( $entry, '12' ),
        'comments' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SS - hardwood restoration lead
add_action( 'gform_after_submission_16', 'post_to_third_party_16', 10, 2 );

function post_to_third_party_16( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = '73102e16-aa51-42f3-a9ec-ae6361c3f7a2';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'rooms' => rgar( $entry, '11' ),
        'date' => rgar( $entry, '13' ),
        'time' => rgar( $entry, '12' ),
        'comments' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SS - Measurement Lead
add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

function post_to_third_party_3( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = '8d469749-7a36-4b6d-8384-5874d867f396';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'zip' => rgar( $entry, '5' ),
        'product type' => rgar( $entry, '11' ),
        'date' => rgar( $entry, '13' ),
        'time' => rgar( $entry, '12' ),
        'comments' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SS - Military Lead
add_action( 'gform_after_submission_18', 'post_to_third_party_18', 10, 2 );

function post_to_third_party_18( $entry, $form ) {
    $baseURI = 'https://app-3QNH5ZKKGG.marketingautomation.services/webforms/receivePostback/MzawMDE3MjUwAgA/';
    $endpoint = 'e5bc60c2-4f61-4d60-ac55-50d63c118386';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '2' ),
        'terms' => rgar( $entry, '7' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

// Disable Updraft and Wordfence on localhost
add_action( 'init', function () {
    if ( !is_admin() && ( strpos( get_site_url(), 'localhost' ) > -1 ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
        if ( file_exists( $updraft ) ) deactivate_plugins( [ $updraft ] );

        $wordfence = WP_PLUGIN_DIR . '/wordfence/wordfence.php';
        if ( file_exists( $wordfence ) ) deactivate_plugins( [ $wordfence ] );
    }
    register_shortcodes();
} );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );



//* Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );
// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Disable gravity forms editor
function remove_gf_notification_visual_editor($settings, $editor_id)
{
    if ($editor_id === 'gform_notification_message') {
    $settings['tinymce'] = false;
}
    return $settings;
}

add_filter('wp_editor_settings', 'remove_gf_notification_visual_editor', 10, 2);


add_action( 'wp_head', function() {
?>
<script>
(function($) {
  $(document).on('facetwp-refresh', function() {
    if (FWP.loaded) {
      FWP.set_hash();
      window.location.reload();
      return false;
    }
  });
})(jQuery);
</script>
<?php
}, 100 );

function remove_unwanted_css(){
    wp_dequeue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
    wp_dequeue_script( 'jquery-throttle', get_template_directory_uri() . '/js/jquery.throttle.min.js' );
}
add_filter('wp_print_styles', 'remove_unwanted_css');

function add_font_awesome2()
{
    wp_enqueue_style( 'font-awesome2', get_stylesheet_directory_uri().'/resources/font-awesome.min.css' );
}
add_action( 'wp_enqueue_scripts', 'add_font_awesome2' );




add_filter( 'script_loader_tag', 'wsds_defer_scripts', 10, 3 );
function wsds_defer_scripts( $tag, $handle, $src ) {

	// The handles of the enqueued scripts we want to defer
	$defer_scripts = array( 		
		'jquery-migrate',
		'imagesloaded',
        'bootstrap',
        'jquery',
        'wp-api',
        //'jquery-bxslider',
        'fl-builder-layout-2',
        'fts_powered_by_js',
        'fts-global', 
        'wpgmza_data',
        //'child-script',
        //'slick', 
        'cookie',
        'jquery-throttle',
        'imagesloaded',
        'jquery-magnificpopup',
        'bootstrap',
        'fl-automator',
	);

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }
    
    return $tag;
} 

add_filter( 'script_loader_tag', 'wsds_async_scripts', 10, 3 );
function wsds_async_scripts( $tag, $handle, $src ) {

	// The handles of the enqueued scripts we want to defer
	$defer_scripts = array( 		
		'jquery-migrate',
		'imagesloaded',
        'bootstrap',
        'jquery',
        'wp-api',
        //'jquery-bxslider',
        'fl-builder-layout-2',
        'fts_powered_by_js',
        'fts-global', 
        'wpgmza_data',
        //'slick', 
        //'child-script',
        'cookie',
        'jquery-throttle',
        'imagesloaded',
        'jquery-magnificpopup',
        'bootstrap',
        'fl-automator',
	);

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" async="async" type="text/javascript"></script>' . "\n";
    }
    
    return $tag;
} 


// add_filter( 'style_loader_tag',  'remove_https_http', 10, 4 );
// function remove_https_styles( $html, $handle, $href, $media ){
//     $handles = array('twentysixteen-fonts', 'open-sans');
//     if( in_array( $handle, $handles ) ){
//         $html = str_replace('https:', '', $html);   
//     }
//     return $html;
// }

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

function addCartbutton($atts = [], $content = null, $tag = '')
{
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    echo '<div class="cart"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a><div>';
    
}
add_shortcode( 'addCartbutton', 'addCartbutton' );

?>